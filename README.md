# City data

This repository contains a script to download graphs of major cities, stored in the `cities.txt`
and save them into protocol buffers format inside the `data` folder.

The actual `.pbf` file can then be rendered by the [City Mapper](https://gitlab.com/ambient-ridge-llc/areahub/clients/city-mapper) tool.

Note: Previously the `.pbf` file were stored inside this repository, but github removed them.
I assume they were too large.

## How to use it

One time setup:

```
git clone https://gitlab.com/rajinwonderland/index-large-cities.git
cd index-large-cities
npm install
```

Then you should be able to download all cities from `cities.txt` file by running

```
node index.js
```
